﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt
{
    public class Auto : Vozilo
    {
        public Auto(string nazivVoz, string bojaVoz, int snagaVoz, int brojVrata) : base(nazivVoz, bojaVoz, snagaVoz)
        {
            this.brojVrata = brojVrata;
        }

        public override string Ispis()
        {
            return base.Ispis() + ", " + brojVrata;
        }

        public override string ToString()
        {
            return Ispis();
        }

        public int brojVrata { get; set; }
    }
}
