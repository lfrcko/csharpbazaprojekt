﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt {
    public partial class Form4 : Form {
        private delegate void Loading();
        private delegate void Delegat(string poruka);
        private event Delegat IspisPoruke;
        private int broj = 10;
        private int racunaj = 0;
        int[] brojevi = new int[3];
        Polje<double> mojePolje = new Polje<double>(5);
        public Form4() {
            InitializeComponent();
            mojePolje.Unesi(2.313);
            mojePolje.Unesi(3);
            MessageBox.Show(mojePolje.Dohvati(0).ToString());
            Delegat delegat = Ispisi;
            delegat("Radi delegat!");
            Auto auto = new Auto("Zastava", "crvena", 80, 3);
            var smart = new { nazivVoz = "Porsche", bojaVoz = "siva", snagaVoz = "100", brojVrata = "5" };
            if (smart.Equals(new { nazivVoz = "Porsche", bojaVoz = "siva", snagaVoz = "100", brojVrata = "4" })) {
                MessageBox.Show("Auti su isti.");
            } else {
                MessageBox.Show("Auti nisu isti.");
            }
            //auto.IspisPoruke += Ispisi;
            //CPU cpu = new CPU (1523.2);
            //CPU.Procesor procesor = cpu.new Procesor(8, "AMD");
            Vanjska.Unutarnja.Ispis();
            new Thread(() => {
                lock ((object)broj) {
                    broj = broj + 10;
                    Thread.CurrentThread.IsBackground = true;
                    Console.WriteLine("Ovo se odvija u novoj dretvi.");
                    int a = 100 + 100 + 200;
                    MessageBox.Show(broj.ToString());
                    MessageBox.Show(a.ToString());
                }
            }).Start();
            Povecavaj();
        }

        private void Ucitaj() {
            lock ((object)broj) {
                for (int i = 0; i <= 101; i++) {
                    broj++;
                    if (progressBar.InvokeRequired) {
                        progressBar.Invoke(new Action(Ucitaj));
                    }
                    else {
                        progressBar.Value = i;
                    }
                    Thread.Sleep(100);
                    if (i == 99) {
                        MessageBox.Show("Podatak ucitan");
                        MessageBox.Show(broj.ToString());
                        Thread.CurrentThread.Abort();
                    }
                }
            }
        }

        public async Task Povecavaj() {
            Task<int> umanji = Umanji();
            for(int i=1;i<=100;i++) {
                racunaj = racunaj + 1;
            }
            await umanji;
            MessageBox.Show("Povecaj i umanji: " + racunaj.ToString());
        }

        public async Task<int> Umanji() {
            for (int i=1;i<=100;i++) {
                racunaj = racunaj - 1;
            }
            return 1;
        }

        private void btnLoad_Click(object sender, EventArgs e) {
            Thread t1 = new Thread(Ucitaj);
            t1.IsBackground = true;
            t1.Start();
            /*
            Loading load = Ucitaj;
            load.BeginInvoke(new AsyncCallback(Ucitano), null);
            */
        }

        private void Ucitano(IAsyncResult iar) {
            MessageBox.Show("Podatak ucitan");
        }

        private void Ispisi(string poruka) {
            if (IspisPoruke != null) {
                MessageBox.Show("Ispis poruke");
            }
            MessageBox.Show("Poruka: {0}", poruka);
        }
    }
}
