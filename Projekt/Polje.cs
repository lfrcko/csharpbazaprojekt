﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt {
    internal class Polje<T> {
        public Polje(int velicina) {
            polje = new T[velicina];
            maxVelicina = velicina;
        }

        public bool Unesi(T unos) {
            if (index < maxVelicina) {
                polje[index] = unos;
                index++;
                return true;
            } else {
                return false;
            }
        }

        public bool Obrisi(int pozicija) {
            if (pozicija < maxVelicina && pozicija >= 0) {
                polje[pozicija] = default(T)!;
                return true;
            } else {
                return false;
            }
        }

        public T Dohvati(int pozicija) {
            if (pozicija < maxVelicina && pozicija >= 0) {
                return polje[pozicija];
            }
            else return default(T)!;
        }

        private T[] polje;
        private int index = 0;
        private int maxVelicina;
    }
}
