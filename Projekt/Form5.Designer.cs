﻿namespace Projekt {
    partial class Form5 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnZatvori = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.txtBoxNazivVozila = new System.Windows.Forms.TextBox();
            this.lblNazivKamiona = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnZatvori
            // 
            this.btnZatvori.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnZatvori.Location = new System.Drawing.Point(14, 140);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(213, 33);
            this.btnZatvori.TabIndex = 39;
            this.btnZatvori.Text = "Zatvori porzor";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnObrisi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnObrisi.Location = new System.Drawing.Point(14, 86);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(213, 33);
            this.btnObrisi.TabIndex = 36;
            this.btnObrisi.Text = "Obrisi";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // txtBoxNazivVozila
            // 
            this.txtBoxNazivVozila.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxNazivVozila.Location = new System.Drawing.Point(14, 35);
            this.txtBoxNazivVozila.Name = "txtBoxNazivVozila";
            this.txtBoxNazivVozila.Size = new System.Drawing.Size(213, 29);
            this.txtBoxNazivVozila.TabIndex = 33;
            // 
            // lblNazivKamiona
            // 
            this.lblNazivKamiona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNazivKamiona.Location = new System.Drawing.Point(50, 9);
            this.lblNazivKamiona.Name = "lblNazivKamiona";
            this.lblNazivKamiona.Size = new System.Drawing.Size(134, 23);
            this.lblNazivKamiona.TabIndex = 30;
            this.lblNazivKamiona.Text = "Naziv vozila";
            this.lblNazivKamiona.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 192);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.btnObrisi);
            this.Controls.Add(this.txtBoxNazivVozila);
            this.Controls.Add(this.lblNazivKamiona);
            this.Name = "Form5";
            this.Text = "Form5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button btnZatvori;
        private Button btnObrisi;
        private TextBox txtBoxNazivVozila;
        private Label lblNazivKamiona;
    }
}