﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt {
    internal class DeserializeIznimka : Exception {
        public DeserializeIznimka() : base("Greška pri deserijalizaciji dokumenta.") { }
        public DeserializeIznimka(string message) : base(message) { }
    }
}
