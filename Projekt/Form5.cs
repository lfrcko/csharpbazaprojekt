﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt {
    public partial class Form5 : Form {
        private AutoBaza autoBaza;
        public KamionBaza kamionBaza;

        public Form5() {
            InitializeComponent();
            autoBaza = new AutoBaza();
            kamionBaza = new KamionBaza();
        }

        private void btnObrisi_Click(object sender, EventArgs e) {
            Auto auto = autoBaza.DohvatiAuto(txtBoxNazivVozila.Text);
            Kamion kamion = kamionBaza.DohvatiKamion(txtBoxNazivVozila.Text);
            if (auto != null && kamion == null) {
                autoBaza.ObrisiAuto(auto);
                this.Close();
            }
            else if (auto == null && kamion != null) {
                kamionBaza.ObrisiKamion(kamion);
                this.Close();
            }
            else if (auto != null && kamion != null) {
                autoBaza.ObrisiAuto(auto);
                kamionBaza.ObrisiKamion(kamion);
                this.Close();
            }
            else {
                MessageBox.Show("Nije pronađeno nijedno vozilo s zadanim nazivom.");
            }
        }

        private void btnZatvori_Click(object sender, EventArgs e) {
            this.Hide();
        }
    }
}
