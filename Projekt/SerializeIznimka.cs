﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt {
    internal class SerializeIznimka : Exception {
        public SerializeIznimka() : base("Greška pri serijalizaciji dokumenta.") { }
        public SerializeIznimka(string message) : base(message) { }
    }
}
