﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public partial class Form3 : Form
    {
        public KamionBaza kamionBaza;
        public Kamion? NoviKamion { get; set; }
        public Form3()
        {
            InitializeComponent();
        }

        private void btnUnesi_Click(object sender, EventArgs e) {
            NoviKamion = new Kamion(txtBoxNazivKamiona.Text, txtBoxBojaKamiona.Text, int.Parse(txtBoxSnagaKamiona.Text), int.Parse(txtBoxKilometri.Text));
            this.Hide();
        }

        private void btnZatvori_Click(object sender, EventArgs e) {
            this.Hide();
        }

        private void btnDohvat_Click(object sender, EventArgs e) {
            kamionBaza = new KamionBaza();
            Kamion dohvat = kamionBaza.DohvatiKamion(txtBoxNazivKamiona.Text);
            if (dohvat != null) {
                txtBoxBojaKamiona.Text = dohvat.bojaVoz;
                txtBoxSnagaKamiona.Text = dohvat.snagaVoz.ToString();
                txtBoxKilometri.Text = dohvat.prijedeniKilometri.ToString();
                MessageBox.Show("Zapis dohvaćen.");
            }
            else {
                MessageBox.Show("Zapis nije pronađen.");
            }
        }

        private void btnPromijena_Click(object sender, EventArgs e) {
            NoviKamion = new Kamion(txtBoxNazivKamiona.Text, txtBoxBojaKamiona.Text, int.Parse(txtBoxSnagaKamiona.Text), int.Parse(txtBoxKilometri.Text));
            this.Hide();
        }
    }
}
