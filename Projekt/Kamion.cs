﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt
{
    public class Kamion : Vozilo
    {
        public Kamion(string nazivVoz, string bojaVoz, int snagaVoz, int prijedeniKilometri) : base(nazivVoz, bojaVoz, snagaVoz)
        {
            this.prijedeniKilometri = prijedeniKilometri;
        }

        public override string Ispis()
        {
            return base.Ispis() + ", " + prijedeniKilometri;
        }

        public override string ToString()
        {
            return Ispis();
        }

        public int prijedeniKilometri { get; set; }
    }
}
