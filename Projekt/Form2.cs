﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public partial class Form2 : Form
    {
        public AutoBaza autoBaza;
        public Auto? NoviAuto { get; set; }
        public Form2()
        {
            InitializeComponent();
        }

        private void btnUnesi_Click(object sender, EventArgs e)
        {
            NoviAuto = new Auto(txtBoxNazivAuta.Text, txtBoxBojaAuta.Text, int.Parse(txtBoxSnagaAuta.Text), int.Parse(txtBoxBrojVrata.Text));
            this.Hide();
        }

        private void buttonZatvori_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnDohvat_Click(object sender, EventArgs e) {
            autoBaza = new AutoBaza();
            Auto dohvat = autoBaza.DohvatiAuto(txtBoxNazivAuta.Text);
            if (dohvat != null) {
                txtBoxBojaAuta.Text = dohvat.bojaVoz;
                txtBoxSnagaAuta.Text = dohvat.snagaVoz.ToString();
                txtBoxBrojVrata.Text = dohvat.brojVrata.ToString();
                MessageBox.Show("Zapis dohvaćen.");
            }
            else {
                MessageBox.Show("Zapis nije pronađen.");
            }
        }

        private void btnPromijena_Click(object sender, EventArgs e) {
            NoviAuto = new Auto(txtBoxNazivAuta.Text, txtBoxBojaAuta.Text, int.Parse(txtBoxSnagaAuta.Text), int.Parse(txtBoxBrojVrata.Text));
            this.Hide();
        }
    }
}
