﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt {
    public class CPU {

        CPU (double cijena) {
            this.cijena = cijena;
        }

        public double cijena { get; set; }

        public class Procesor {
            Procesor (double brojJezgri, String proizvodac) {
                this.brojJezgri = brojJezgri;
                this.proizvodac = proizvodac;
            }

            private double brojJezgri { get; set; }
            private string proizvodac { get; set; }
        }

        public class RAM {
            RAM (double memorija, double clockSpeed, string proizvodac) {
                this.memorija = memorija;
                this.clockSpeed = clockSpeed;
                this.proizvodac = proizvodac;
            }

            private double memorija { get; set; }
            private double clockSpeed { get; set; }
            private String proizvodac { get; set; }
        }
    }
}
