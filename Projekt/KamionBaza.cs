﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Collections;

namespace Projekt
{
    public class KamionBaza
    {
        private ArrayList bazaKamiona;

        public KamionBaza()
        {
            bazaKamiona = new ArrayList();
            Deserialize();
        }

        public void DodajKamion(Kamion kamion)
        {
            if (DohvatiKamion(kamion.nazivVoz) == null) {
                bazaKamiona.Add(kamion);
                Serialize();
                MessageBox.Show("Kamion dodan.");
            }
            else {
                MessageBox.Show("Kamion s zadanim nazivom već postoji.");
            }
        }

        public void ObrisiKamion(Kamion kamion) {
            bazaKamiona.Remove(kamion);
            Serialize();
            MessageBox.Show("Kamion obrisan.");
        }

        public Kamion DohvatiKamion(string nazivKamiona) {
            foreach (Kamion kamion in bazaKamiona) {
                if (nazivKamiona == kamion.nazivVoz) {
                    return kamion;
                }
            }
            return null!;
        }

        public bool Promijeni(Kamion kamion) {
            foreach (Kamion a in bazaKamiona) {
                if (a.nazivVoz == kamion.nazivVoz) {
                    a.nazivVoz = kamion.nazivVoz;
                    a.bojaVoz = kamion.bojaVoz;
                    a.snagaVoz = kamion.snagaVoz;
                    a.prijedeniKilometri = kamion.prijedeniKilometri;
                    Serialize();
                    MessageBox.Show("Kamion izmjenjen.");
                    return true;
                }
            }
            MessageBox.Show("Kamion nije pronaden.");
            return false;
        }

        public ArrayList DohvatiKamione()
        {
            return bazaKamiona;
        }

        public void Serialize()
        {
            try
            {
                string kamioniInJson = JsonSerializer.Serialize(bazaKamiona);
                using (StreamWriter KamioniUnos = new(@"D:\Projekt\Projekt\Kamioni.txt"))
                {
                    KamioniUnos.WriteLine(kamioniInJson);
                }
            }
            catch (SerializeIznimka ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Deserialize()
        {
            try
            {
                string json = File.ReadAllText(@"D:\Projekt\Projekt\Kamioni.txt");
                List<Kamion> ListaKamiona = JsonSerializer.Deserialize<List<Kamion>>(json)!;
                foreach (Kamion k in ListaKamiona)
                {
                    bazaKamiona.Add(k);
                }
            }
            catch (DeserializeIznimka ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
