﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt
{
    public abstract class Vozilo
    {
        public Vozilo(string nazivVoz, string bojaVoz, int snagaVoz)
        {
            this.nazivVoz = nazivVoz;
            this.bojaVoz = bojaVoz;
            this.snagaVoz = snagaVoz;
        }

        public virtual string Ispis()
        {
            return nazivVoz + ", " + bojaVoz + ", " + snagaVoz;
        }

        public string getNazivVoz()
        {
            return nazivVoz;
        }

        public string nazivVoz { get; set; }
        public string bojaVoz { get; set; }
        public int snagaVoz { get; set; }
    }
}
