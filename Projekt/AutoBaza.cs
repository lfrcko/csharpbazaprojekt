﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace Projekt
{
    public class AutoBaza
    {
        private List<Auto> bazaAuta;

        public AutoBaza()
        {
            bazaAuta = new List<Auto>();
            Deserialize();
        }

        public void DodajAuto(Auto auto)
        {
            if (DohvatiAuto(auto.nazivVoz) == null) {
                bazaAuta.Add(auto);
                Serialize();
                MessageBox.Show("Auto dodan.");
            }
            else {
                MessageBox.Show("Auto s zadanim nazivom već postoji.");
            }
        }

        public void ObrisiAuto(Auto auto) {
            bazaAuta.Remove(auto);
            Serialize();
            MessageBox.Show("Auto obrisan.");
        }

        public Auto DohvatiAuto(string nazivAuta) {
            foreach (Auto auto in bazaAuta) {
                if (nazivAuta == auto.nazivVoz) {
                    return auto;
                }
            }
            return null!;
        }

        public bool Promijeni(Auto auto) {
            foreach (Auto a in bazaAuta) {
                if (a.nazivVoz == auto.nazivVoz) {
                    a.nazivVoz = auto.nazivVoz;
                    a.bojaVoz = auto.bojaVoz;
                    a.snagaVoz = auto.snagaVoz;
                    a.brojVrata = auto.brojVrata;
                    Serialize();
                    MessageBox.Show("Auto izmjenjen.");
                    return true;
                }
            }
            MessageBox.Show("Auto nije pronaden.");
            return false;
        }

        public IEnumerable<Auto> DohvatiAute()
        {
            return
              from a in bazaAuta
              orderby a.nazivVoz
              select a;
        }

        public void Serialize()
        {
            try
            {
                string autiInJson = JsonSerializer.Serialize(bazaAuta);
                using (StreamWriter AutiUnos = new(@"D:\Projekt\Projekt\Auti.txt"))
                {
                    AutiUnos.WriteLine(autiInJson);
                }
            }
            catch (SerializeIznimka ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Deserialize()
        {
            try
            {
                string json = File.ReadAllText(@"D:\Projekt\Projekt\Auti.txt");
                bazaAuta = JsonSerializer.Deserialize<List<Auto>>(json)!;
            }
            catch (DeserializeIznimka ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
