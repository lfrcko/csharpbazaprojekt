﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt {
    internal class Vanjska {
        public static string tekst = "Ja sam vanjska klasa!";

        public static class Unutarnja {
            public static void Ispis() {
                MessageBox.Show(Vanjska.tekst);
            }
        }
    }
}
